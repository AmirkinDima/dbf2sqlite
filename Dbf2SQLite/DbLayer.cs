﻿using System;
using System.IO;
using System.Data.SQLite;

namespace Plat_doc2
{
    // DbLayer
    public class DbLayer : IDisposable
    {
        private static DbLayer instance = null;
        public static DbLayer Instance {
            get
            {
                return instance ?? (instance = new DbLayer());
            }
        }

        private readonly SQLiteConnection _db;

        public DbLayer()
        {
            try
            {
                if (!File.Exists("db.sqlite"))
                {
                    SQLiteConnection.CreateFile("db.sqlite");
                } 
                
                _db = new SQLiteConnection("Data Source=db.sqlite;Version=3;");
                _db.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public SQLiteConnection GetDb()
        {
            return _db;
        }

        public void Dispose()
        {
            _db.Close();
        }
    }
}
