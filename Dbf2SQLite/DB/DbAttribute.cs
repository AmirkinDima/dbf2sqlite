﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Plat_doc2.DB
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DbAttribute : Attribute
    {
        public string TableName { get; set; }
        
        public DbAttribute()
        {
            TableName = "Имя таблицы";
        }

    }
}