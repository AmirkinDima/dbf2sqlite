﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace Plat_doc2.DB
{
    public class Field
    {
        public string ColumnName { get; set; }
        public string Type { get; set; }

    }

    public class DbUsers
    {
        private SQLiteConnection _db;

        public string DataTable = "Users";
        public List<Field> Fields = new List<Field>()
        {
            new Field()
            {
                ColumnName = "Id",
                Type = "int"
            },
            new Field
            {
                ColumnName = "Name",
                Type = "varchar(20)"
            }
            
        }; 

        public DbUsers( SQLiteConnection db)
        {
            _db = db;
        }

        public bool CreateTable()
        {
            var sql = "create table users (name varchar(20), id int)";
            var command = new SQLiteCommand(sql, _db);
            command.ExecuteNonQuery();

            return true;
        }

    }
}
