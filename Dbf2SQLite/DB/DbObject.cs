﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Plat_doc2;
using Plat_doc2.DB;

namespace Dbf2SQLite.DB
{
    public class DbObject<T> where T : new()
    {
        private readonly SQLiteConnection _db;
        private readonly T tableObject;

        public DbObject()
        {
            tableObject = new T();
            _db = DbLayer.Instance.GetDb();
        }
        
        public bool CreateTable()
        {
            var sql = "create table {tableName} {fields}"
                .Replace("{tableName}", GetTableName())
                .Replace("{fields}", GetRealFieldsString());
            var command = new SQLiteCommand(sql, _db);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("[Предупреждение]: Таблица с этим именем {0} уже существует.", GetTableName()));
            }

            return true;
        }

        public bool DbExists()
        {
            // Проверка на существование БД
            var rows = _db.GetSchema("Tables").Select(string.Format("Table_Name = '{0}'", GetTableName())); ;
            
            return rows.Length != 0;
        }

        private string GetTableName()
        {
            var type = tableObject.GetType(); // получение описания типа
            if (Attribute.IsDefined(type, typeof(DbAttribute))) // проверка на существование атрибута
            {
                var attributeValue = Attribute.GetCustomAttribute(type, typeof(DbAttribute)) as DbAttribute; // получаем значение атрибута
                return attributeValue.TableName; // используем значение атрибута для формирования результата
            }
            return "";
        }

        private string GetFieldsString()
        {

            var columns = GetFields();
            var listStr = columns.Select(column => column.Item1 + " " + column.Item2).ToList();

            return "(" + string.Join(",", listStr) + ")";
        }

        private string GetRealFieldsString()
        {

            var columns = GetRealFields();
            var listStr = columns.Select(column => column.Item1 + " " + column.Item2 +
                (column.Item1 == "Id" ? " PRIMARY KEY AUTOINCREMENT" : "")).ToList();

            return "(" + string.Join(",", listStr) + ")";
        }

        private List<Tuple<string, string>> GetRealFields()
        {
            var columns = new List<Tuple<string, string>>();

            var fields = typeof(T).GetProperties();
            
            foreach (var field in fields)
            {
                if (!field.IsDefined(typeof (ColumnAttribute), false)) continue;
                
                var attributeValue = field.GetCustomAttributes(typeof(ColumnAttribute), false)[0] as ColumnAttribute; // получаем значение атрибута
                var name = attributeValue.ColumnName;
                var type = attributeValue.ColumnType;

                if (type == "datetime")
                    type = "int";

                columns.Add(new Tuple<string, string>(name, type));
            }
            return columns;
        }

        private List<Tuple<string, string>> GetFields()
        {
            var columns = new List<Tuple<string, string>>();

            var fields = typeof(T).GetProperties();
            
            foreach (var field in fields)
            {
                if (!field.IsDefined(typeof (ColumnAttribute), false)) continue;
                
                var attributeValue = field.GetCustomAttributes(typeof(ColumnAttribute), false)[0] as ColumnAttribute; // получаем значение атрибута
                var name = attributeValue.ColumnName;
                var type = attributeValue.ColumnType;
                
                columns.Add(new Tuple<string, string>(name, type));
            }
            return columns;
        }

        public List<T> GetRows(string where = "")
        {
            var sql = "Select * from {tableName} {where}"
                .Replace("{tableName}", GetTableName())
                .Replace("{where}", where);

            var command = new SQLiteCommand(sql, _db);
            var reader = command.ExecuteReader();
            var rows = new List<T>();
            
            while (reader.Read())
            {
                var rawRow = reader.GetValues();
                var fieldsList = GetFields();
                
                var row = new T();
                var asm = Assembly.GetEntryAssembly();  
                foreach (var field in fieldsList)
                {
                    try
                    {
                        switch (field.Item2)
                        {
                            case "long":
                                typeof (T).GetProperty(field.Item1)
                                    .SetValue(row, long.Parse(rawRow[field.Item1]), null);
                                break;
                            case "datetime":
                                typeof (T).GetProperty(field.Item1).SetValue(
                                    row, new DateTime(long.Parse(rawRow[field.Item1]))
                                    , null);
                                break;
                            case "text":
                                typeof (T).GetProperty(field.Item1).SetValue(row, rawRow[field.Item1], null);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                
                rows.Add(row);
            }
            return rows;
        }

        public bool InsertRow(T row)
        {
            var result = true;
            var t = row.GetType();
            var values = new List<object>();
            var fields = GetFields();
            try
            {
                foreach (var field in fields)
                {
                    object value;
                    PropertyInfo property;
                    switch (field.Item2)
                    {
                        case "datetime":
                            property = t.GetProperty(field.Item1);
                            value = property != null ? ((DateTime) t.GetProperty(field.Item1).GetValue(row, null)).Ticks : DateTime.Now.Ticks;
                            break;
                        case "text":
                            property = t.GetProperty(field.Item1);
                            value = property != null ? "'" + property.GetValue(row, null) + "'" : "''";

                            break;
                        default:
                            property = t.GetProperty(field.Item1);
                            value = property != null ? t.GetProperty(field.Item1).GetValue(row, null) : 0;
                            
                            break;
                    }

                    if (property != null)
                    {
                        values.Add(property.Name == "Id" ? "null" : value);
                    }
                    else
                    {
                        values.Add(value);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            var sql = "Insert into {tableName} ({fields}) VALUES ({values})"
                .Replace("{tableName}", GetTableName())
                .Replace("{fields}", string.Join(",", GetFields().Select(x => x.Item1)))
                .Replace("{values}", string.Join(",", values));

            try
            {
                var command = new SQLiteCommand(sql, _db);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }
    }
}
