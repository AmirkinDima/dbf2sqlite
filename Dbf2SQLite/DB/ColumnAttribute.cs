﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Plat_doc2.DB
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnAttribute: Attribute
    {
        public string ColumnName { get; set; }
        public string ColumnType { get; set; }
    }
}
