﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Plat_doc2.DB;

namespace Plat_doc2.Models
{
    [Db(TableName = "Client")]
    public class ClientModel : IDataErrorInfo 
    {
        // Порядковый номер
        [Column(ColumnName = "Id", ColumnType = "INTEGER")]
        public long Id { get; set; }

        // Оборот
        [Column(ColumnName = "Turn", ColumnType = "INTEGER")]
        public long Turn { get; set; }

        // Наименование организации
        [Column(ColumnName = "Name", ColumnType = "text")]
        public string Name { get; set; }

        // Лицевой счет
        [Column(ColumnName = "BankBook", ColumnType = "text")]
        public string BankBook { get; set; }

        // ИНН
        [Column(ColumnName = "Inn", ColumnType = "text")]
        public string Inn { get; set; }

        // Банк
        [Column(ColumnName = "BankName", ColumnType = "text")]
        public string BankName { get; set; }

        // БИК
        [Column(ColumnName = "Bik", ColumnType = "text")]
        public string Bik { get; set; }

        // Расчетный счет
        [Column(ColumnName = "CurrentAccount", ColumnType = "text")]
        public string CurrentAccount { get; set; }
        
        // Корреспонденский счет
        [Column(ColumnName = "CorrespondentAccount", ColumnType = "INTEGER")]
        public long CorrespondentAccount { get; set; }

        // Город
        [Column(ColumnName = "City", ColumnType = "text")]
        public string City { get; set; }

        // Адрес
        [Column(ColumnName = "Address", ColumnType = "text")]
        public string Address { get; set; }

        // ОКОХН
        [Column(ColumnName = "Okohn", ColumnType = "long")]
        public long Okohn { get; set; }

        // ОКПО
        [Column(ColumnName = "Okpo", ColumnType = "long")]
        public long Okpo { get; set; }

        // Директор
        [Column(ColumnName = "Director", ColumnType = "text")]
        public string Director { get; set; }

        // Бухгалтер
        [Column(ColumnName = "Buh", ColumnType = "text")]
        public string Buh { get; set; }

        // Телефон Директора
        [Column(ColumnName = "TelephoneDirector", ColumnType = "text")]
        public string TelephoneDirector { get; set; }

        // Телефон Бухгалтера
        [Column(ColumnName = "TelephoneBuh", ColumnType = "text")]
        public string TelephoneBuh { get; set; }

        // Описание
        [Column(ColumnName = "Description", ColumnType = "text")]
        public string Description { get; set; }

        // Начальная дата
        [Column(ColumnName = "BeginDate", ColumnType = "datetime")]
        public DateTime BeginDate { get; set; }

        // Конечная дата
        [Column(ColumnName = "EndDate", ColumnType = "datetime")]
        public DateTime EndDate { get; set; }

        // === На начало периода

        // Сумма дебета
        [Column(ColumnName = "BeginDebet", ColumnType = "real")]
        public double BeginDebet { get; set; }

        // Сумма дебета страхового сбора
        [Column(ColumnName = "BeginDebetInsurance", ColumnType = "real")]
        public double BeginDebetInsurance { get; set; }

        // Сумма дебета страхового сбора с наличных продаж
        [Column(ColumnName = "BeginDebetCash", ColumnType = "real")]
        public double BeginDebetCash { get; set; }

        // Сумма кредита на начало периода
        [Column(ColumnName = "BeginCredit", ColumnType = "real")]
        public double BeginCredit { get; set; }

        // Сумма кредита страхового сбора
        [Column(ColumnName = "BeginCreditInsurance", ColumnType = "real")]
        public double BeginCreditInsurance { get; set; }

        // === За период

        // Сумма дебета
        [Column(ColumnName = "PeriodDebet", ColumnType = "real")]
        public double PeriodDebet { get; set; }

        // Сумма дебета страхового сбора
        [Column(ColumnName = "PeriodDebetInsurance", ColumnType = "real")]
        public double PeriodDebetInsurance { get; set; }

        // Сумма дебета страхового сбора с наличных продаж
        [Column(ColumnName = "PeriodDebetCash", ColumnType = "real")]
        public double PeriodDebetCash { get; set; }

        // Сумма кредит на начало периода
        [Column(ColumnName = "PeriodCredit", ColumnType = "real")]
        public double PeriodCredit { get; set; }

        // Сумма кредита страхового сбора
        [Column(ColumnName = "PeriodCreditInsurance", ColumnType = "real")]
        public double PeriodCreditInsurance { get; set; }

        // === На конец периода

        // Сумма дебета
        [Column(ColumnName = "EndDebet", ColumnType = "real")]
        public double EndDebet { get; set; }

        // Сумма дебета страхового сбора
        [Column(ColumnName = "EndDebetInsurance", ColumnType = "real")]
        public double EndDebetInsurance { get; set; }

        // Сумма дебета страхового сбора с наличных продаж
        [Column(ColumnName = "EndDebetCash", ColumnType = "real")]
        public double EndDebetCash { get; set; }

        // Сумма кредит на начало периода
        [Column(ColumnName = "EndCredit", ColumnType = "real")]
        public double EndCredit { get; set; }

        // Сумма кредита страхового сбора
        [Column(ColumnName = "EndCreditInsurance", ColumnType = "real")]
        public double EndCreditInsurance { get; set; }


        public string Error
        {
            get { return "ClientModelError"; }
        }

        public string this[string columnName]
        {
            get { return String.Empty; }
        }
    }
}
