﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using Dbf2SQLite.DB;
using Plat_doc2;
using Plat_doc2.Models;


namespace Dbf2SQLite
{
    class Program
    {
        static void Main(string[] args)
        {
            ConvertedClientDb();
        }

        public static void ConvertedClientDb()
        {
            var FILE_NAME = "client.xml";
            //var listModels = new List<ClientModel>();
            if (File.Exists("db.sqlite"))
            {
                File.Delete("db.sqlite");
            }

            var doc = new XPathDocument(FILE_NAME);

            var navigator = doc.CreateNavigator();
            var modeIterator = navigator.Select("Root/Records/Record");
            var db = DbLayer.Instance;
            var obj = new DbObject<ClientModel>();
            obj.CreateTable();
            
            while (modeIterator.MoveNext())
            {
                var client = new ClientModel();
                var childs = modeIterator.Current.Select("./*");

                while (childs.MoveNext())
                {
                    var item = childs.Current;
                    long result;
                    switch (item.Name)
                    {
                        case "NAME_1":
                        case "NAME_2":
                        case "NAME_3":
                            client.Name += ConvertFrom866To1251(item.Value) + " ";
                            break;

                        case "BANK_1":
                        case "BANK_2":
                            client.Name += ConvertFrom866To1251(item.Value) + " ";
                            break;
                        case "INN":
                            client.Inn =  ConvertFrom866To1251(item.Value);
                            break;
                        case "BIK":
                            client.Bik = ConvertFrom866To1251(item.Value);
                            break;
                        case "R_SCHET":
                            client.CurrentAccount = ConvertFrom866To1251(item.Value);
                            break;
                        case "K_SCHET":
                            long.TryParse(item.Value, out result);
                            client.CorrespondentAccount = result;
                            break;
                        case "GOROD":
                            client.City = ConvertFrom866To1251(item.Value);
                            break;
                        case "ADRES":
                            client.Address = ConvertFrom866To1251(item.Value);
                            break;
                        case "COD_OKONH":
                            long.TryParse(item.Value, out result);
                            client.Okohn = result;
                            break;
                        case "COD_OKPO":
                            long.TryParse(item.Value, out result);
                            client.Okpo = result;
                            break;
                        case "DIREKTOR":
                            client.Director = ConvertFrom866To1251(item.Value);
                            break;
                        case "GL_BUHG":
                            client.Buh = ConvertFrom866To1251(item.Value);
                            break;
                        case "TELEFON_DI":
                            client.TelephoneDirector = ConvertFrom866To1251(item.Value);
                            break;
                        case "TELEFON_BU":
                            client.TelephoneBuh = ConvertFrom866To1251(item.Value);
                            break;
                        case "NOTE":
                            client.Description = ConvertFrom866To1251(item.Value);
                            break;
                        case "C_NTEG3":
                            long.TryParse(item.Value, out result);
                            client.Turn = result;
                            break;
                        case "C_DBEG":
                            //TODO: Научить конвертировать в дату!
                            DateTime date;
                            DateTime.TryParse(ConvertFrom866To1251(item.Value), out date);
                            client.BeginDate = date;
                            break;
                        case "C_DEND":
                            //TODO: Научить конвертировать в дату!
                            break;
                    }
                }
                obj.InsertRow(client);
            }
        }

        private static void testConverting(string line)
        {
            Debug.WriteLine("testConverting");
            //var bytes = Encoding.GetEncoding(866).GetBytes(line);
            Debug.WriteLine(line);
            var coding = Encoding.GetEncoding(866);
            var coding2 = Encoding.GetEncoding(1251);
            var bytes = coding.GetBytes(line);
            Debug.WriteLine(bytes);
            //var newb = Encoding.Convert(Encoding.GetEncoding(866), Encoding.GetEncoding(1251), bytes);
            var newb = Encoding.Convert(coding, coding2, bytes);
            Debug.WriteLine(newb);
            var result = coding2.GetString(newb);
            Debug.WriteLine(result);
            var a = 1 + 1;
        }


        public static string ConvertFrom866To1251(string line)
        {
            return line;
            var bytes = Encoding.GetEncoding(1251).GetBytes(line);
            var newb= Encoding.Convert(Encoding.GetEncoding(866), Encoding.GetEncoding(1251), bytes);
            return Encoding.GetEncoding(1251).GetString(newb);
        }
    }
}
